Data-only repo for hosting static web pages for several of my Blockland stats pages

Pages can be viewed here:

[https://queuenard.gitlab.io/blockland-stats/index.html](https://queuenard.gitlab.io/blockland-stats/index.html)

